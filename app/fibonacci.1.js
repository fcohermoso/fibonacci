const fibonacci = function ( number ) {
    if (number < 1)
        return 0;
        
    var previous_first = 0, previous_second = 1, next = 1;

    for(var i = 2; i <= number; i++) {
        next = previous_first + previous_second;
        previous_first = previous_second;
        previous_second = next;
    }
    return next;
}

module.exports.fibonacci = fibonacci;

