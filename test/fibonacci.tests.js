const expect = require("chai").expect;
const { fibonacci } = require( "../app/fibonacci" );

describe( "Fibonacci tests: ", function() {
	it( "fibonacci( 0 ) should be 0", function() {
		expect( fibonacci( 0 ) ).to.be.equal( 0 );
	});

	it( "fibonacci( 1 ) should be 1", function() {
		expect( fibonacci( 1 ) ).to.be.equal( 1 );
	});
	
	it( "fibonacci( 2 ) should be 1", function() {
		expect( fibonacci( 2 ) ).to.be.equal( 1 );
	});
	
	it( "fibonacci( 3 ) should be 2", function() {
		expect( fibonacci( 3 ) ).to.be.equal( 2 );
	});
	  
	it( "fibonacci( 10 ) should be 55", function() {
		expect( fibonacci( 10 ) ).to.be.equal( 55 );
  	});
});